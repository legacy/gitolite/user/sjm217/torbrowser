*** 0. Installing Windows ***
Install Windows 7 64-bit, install all updates and service packs.
If you're running a VM, make a snapshot of this clean Windows install.

*** 1. Installing MinGW ***
Download the latest version of MinGW from the following URI:
	http://sourceforge.net/projects/mingw/files/latest/download/

Run the downloaded installer. The installer requires an internet connection to download data.
Since you just downloaded the installer it's fine to use the pre-packaged repository catalogues.
Accept the license agreement if you agree.
Install to the default location (C:\MinGW)
You can choose to create shortcuts in the Start Menu if you so wish.

For the components, select the following:
	C Compiler
	C++ Compiler
	MSYS Basic System
	MinGW Developer Toolkit

Wait until the installation sequence has finished.
Once it finished installing, you can choose to view the install log, but you don't need to so.

Start MinGW Shell and run the following command:
	mingw-get install msys-wget

*** 2. Installing CMake ***
Download CMake from the following URI:
	http://www.cmake.org/files/v2.8/cmake-2.8.8-win32-x86.exe
The latest version can be found at:
	http://www.cmake.org/cmake/resources/software.html

Run the downloaded installer.
Accept the license agreement if you agree.

You don't need to add CMake to our system's PATH.
Change the install location to C:\MinGW\msys\1.0.
You don't need to add a shortcut in the Start Menu.
Wait until the installation sequence has finished.

*** 3. Installing MsysGit ***
Download MsysGit from the following URI:
	https://msysgit.googlecode.com/files/Git-1.7.11-preview20120710.exe
The latest version can be found at:
	https://code.google.com/p/msysgit/downloads/list

Run the downloaded installer.
Accept the license agreement if you agree.
The install location does not matter. You can keep the default. (C:\Program Files (x86)\Git)
Choose the components as desired. But do *not* choose to associate .sh files to be run with Bash.
You can choose to create shortcuts in the Start Menu if you so wish.

If you want to use Git from inside MinGW then choose to run Git from the Windows Command Prompt
Choose the default option 'Checkout Windows-style, commit Unix-style line endings.'

Wait until the installation sequence has finished.
Once it finished installing, you can choose to view the release notes, but you don't need to so.

*** 4. Installing Qt ***
Download Qt 4.8.1 from the following URI:
	http://releases.qt-project.org/qt4/source/qt-win-opensource-4.8.1-mingw.exe

The latest version can be found at (use the MinGW version):
	http://qt.nokia.com/downloads
But you must use the same version as specified by QT_VER in:
	https://gitweb.torproject.org/torbrowser.git/blob/HEAD:/build-scripts/versions.mk

Run the downloaded installer. The loading might take a while.
Accept the license agreement if you agree.
Setting file associations is not necessary for building, but might be handy for debugging.
Use the default destination folder (C:\Qt\4.8.1).
You can use the default start menu folder, but I recommend using a shorter name like 'Qt'.
When asked for the MinGW folder, use the same folder as was used during the MinGW installation (C:\MinGW)
When you click Install, it will most likely give you the following warning:
	There is a problem with your MinGW installation:
	The installer could not find a valid C:\MinGW\include\w32api.h
	(Only versions with W32API 3.13 are supported)
	Do you still want to continue? (Your installation may not work)
In this case you can ignore the warning and choose Yes, since this is a known issue.
Every other warning/error should be taken as such.
Wait until the installation sequence has finished.
You do not need to run the examples/demos nor open the documentation.

*** 5. Installing Visual Studio ***
Download Visual Studio Express 2008 from  the following URI:
	http://download.microsoft.com/download/A/5/4/A54BADB6-9C3F-478D-8657-93B3FC9FE62D/vcsetup.exe
The latest version can be found at:
	http://www.microsoft.com/express/Windows
	
Run the downloaded installer. The installer requires an internet connection to download data.
When asked if you want to Help Improve Setup you can choose not to send information about the setup.
Accept the license agreement if you agree. Displaying RSS content is not necessary.
We don't need the Silverlight runtime, nor SQL Server Express Edition. Disable these.
The install location does not matter. You can keep the default. (C:\Program Files (x86)\Microsoft Visual Studio 9.0)
Wait until the installation sequence has finished.

*** 6. Installing Windows SDK ***
Download the Windows SDK from the following URI:
	http://download.microsoft.com/download/7/A/B/7ABD2203-C472-4036-8BA0-E505528CCCB7/winsdk_web.exe
The latest version can be found at:
	http://msdn.microsoft.com/en-us/windowsserver/bb980924.aspx

Run the downloaded installer. The installer requires an internet connection to download data.
Accept the license agreement if you agree.
The install location does not matter. You can keep the defaults. (C:\Program Files\Microsoft SDKs\Windows\v7.0)
For the components, you can disable Documentation as well as Samples.
Wait until the installation sequence has finished.
Once it finished installing, you can choose to view the release notes, but you don't need to so.

*** 7. Installing DirectX ***
Download the DirectX SDK from the following URI:
	http://download.microsoft.com/download/A/E/7/AE743F1F-632B-4809-87A9-AA1BB3458E31/DXSDK_Jun10.exe
The latest version can be found at:
	http://msdn.microsoft.com/en-us/directx/aa937788.aspx

Start the downloaded installer. It might take a while before the window shows up.
Accept the license agreement if you agree.
It is not necessary to participate in the Customer Experience Improvement Program, but you may do so if you wish.
The install location does not matter. You can keep the defaults. (C:\Program Files (x86)\Microsoft DirectX SDK (June 2010))
For the components, you can disable Documentation as well as Samples and Source Code.
Wait until the installation sequence has finished.

*** 8. Snapshot ***
If you're running a VM, make a snapshot of this clean build environment.

